///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Destynee Fagaragan <djaf6@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   03/20/2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>
#include "palila.hpp"

using namespace std;

namespace animalfarm{
   Palila::Palila (string location, enum Color newColor, enum Gender newGender){
      whereFound = location;
      species = "Loxioides bailleui";
      gender = newGender;
      featherColor = newColor;
      isMigratory = false;
   }

   void Palila::printInfo(){
      cout << "Palila" << endl;
      cout << "   Where Found = [" << whereFound << "]" << endl;
      Bird::printInfo();
   }
}
