///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file fish.hpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author Destynee Fagaragan <djaf6@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   03/21/2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "animal.hpp"

namespace animalfarm{
   class Fish : public Animal {
      public: 

         enum Color scaleColor;
         float favoriteTemp;

         virtual const string speak();

         void printInfo();
   };
}
