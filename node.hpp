///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 205  - Object Oriented Programming
///// Lab 06a - Animal Farm 3
/////
///// @file node.hpp
///// @version 1.0
/////
///// Exports data about all nene birds
/////
///// @author Destynee Fagaragan <djaf6@hawaii.edu>
///// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
///// @date   03/20/2021
/////////////////////////////////////////////////////////////////////////////////

#pragma once
using namespace std;

namespace animalfarm {

   class Node {
      protected:
         Node* next = nullptr;

         friend class SingleLinkedList;
   };
}
