///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file nene.hpp
/// @version 1.0
///
/// Exports data about all nene
///
/// @author Destynee Fagaragan  <djaf6@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   03/23/2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <string>
#include "bird.hpp"

using namespace std;

namespace animalfarm{
   class Nene:public Bird{
      public:
         string tagID;
         Nene (string newTagID, enum Color newColor, enum Gender newGender);

         void printInfo();

         const string speak();
   };
}
